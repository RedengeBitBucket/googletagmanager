<?php

namespace Redenge\GoogleTagManager\AdminModule\Presenters;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Redenge\DB;
use Redenge\Engine\Components\Tab\Bookmark;
use Redenge\Engine\Presenters\BasePresenter;
use Redenge\GoogleTagManager\AdminModule\Model;
use ShopModel;
use Ublaboo\DataGrid\DataGrid;


class GoogleTagManagerPresenter extends BasePresenter
{

	/**
	 * @var DB @inject
	 */
	public $db;

	/**
	 * @var Context
	 */
	protected $database;

	/**
	 * @var \ShopModel
	 */
	protected $shop;


	public function __construct (Context $database, ShopModel $shop)
	{
		$this->database = $database;
		$this->shop = $shop;
	}


	/**
	 * Temporary fix.
	 */
	public function startup()
	{
		require_once(ENGINE_PATH . '/component/admin/interface.php');
		$admin = new \admin($this->db);
		if (!$admin->user_interface->user->login())
			$this->presenter->redirectUrl('/admin');

		parent::startup();
	}


	/**
	 * @param int $id
	 */
	public function actionEdit($id = null)
	{
		$this->shop->google_tag_manager->load(['id' => $id]);
	}


	public function handleDelete($id)
	{
		$this->database->table('google_tag_manager')->where('id = ?', $id)->delete();
		$this['grid']->reload();
	}


		public function createComponentForm()
	{
		$form = new Form;

		$form->addSelect('id_multishop', 'Multishop', $this->getMultishopPairs())
			->setAttribute('class', 'form-control');
		$form->addSelect('id_country', 'Země', $this->getCountryPairs())
			->setAttribute('class', 'form-control');
		$form->addTextArea('head', 'Skript v elementu <HEAD>')
			->setAttribute('class', 'syntax__javascript');
		$form->addTextArea('body', 'Skript v elementu <BODY>')
			->setAttribute('class', 'syntax__javascript');
		$form->addTextArea('layer', 'Zápis datové vrstvy')
			->setAttribute('class', 'syntax__twig');
		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-success mr-2');
		$form->addButton('back', 'Zpět')
			->setAttribute('class', 'btn btn-primary')
			->setAttribute('onclick', sprintf("window.location.href='%s'", $this->link('default')));
		$form->onSuccess[] = [$this, 'onFormSuccess'];

		if ($this->shop->google_tag_manager->id > 0) {
			$defaults = $this->shop->google_tag_manager->getArray();
			$form->setDefaults($defaults);
		}

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'table width="100%" class="table"';
		$renderer->wrappers['label']['container'] = 'th width="15%" class="align-middle"';

		return $form;
	}


	public function onFormSuccess(Form $form)
	{
		$values = $form->getValues();

		foreach ($values as $key => $value) {
			if (!isset($this->shop->google_tag_manager->{$key})) {
				continue;
			}
			$this->shop->google_tag_manager->{$key} = $value;
		}
		$this->shop->google_tag_manager->save();
	}


	public function createComponentGrid()
	{
		$grid = new DataGrid;
		$grid->setDataSource($this->getData());
		$grid->addColumnText('multishop', 'Multishop');
		$grid->addColumnText('country', 'Země');

		$grid->addAction('edit', '', 'GoogleTagManager:edit', ['id' => $grid->getPrimaryKey()])
			->setIcon('pencil')
			->setTitle('editovat')
			->setClass('btn btn-xs btn-default');

		$grid->addAction('delete!', '')
			->setIcon('times')
			->setTitle('odstranit')
			->setClass('btn ajax')
			->setConfirm('Opravdu chcete odstranit položku pro multishop ,,%s" a zemi ,,%s"?', 'multishop', 'country');

		$grid->addToolbarButton('GoogleTagManager:edit', '')->addAttributes([
		])->setClass('btn btn-xs border')
			->setIcon('plus');

		return $grid;
	}


	/**
	 * @return \Nette\Database\ResultSet
	 */
	private function getData()
	{
		return $this->database->table('google_tag_manager')
			->select('google_tag_manager.id, multishop:multishop_attribute.name multishop, country.name country')
			->where('multishop:multishop_attribute.id_language', 1);
	}


	private function getMultishopPairs()
	{
		return $this->database->table('multishop')
			->select('multishop.id, :multishop_attribute.name')
			->where(':multishop_attribute.id_language=1')
			->fetchPairs('id', 'name');
	}


	private function getCountryPairs()
	{
		return $this->database->table('country')
			->select('id, name')
			->fetchPairs('id', 'name');
	}

}
