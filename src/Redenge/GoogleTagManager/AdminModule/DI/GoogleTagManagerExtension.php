<?php

namespace Redenge\GoogleTagManager\AdminModule\DI;

use Nette\Application\IPresenterFactory;
use Nette\DI\CompilerExtension;


/**
 * Description of GoogleTagManagerExtension
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class GoogleTagManagerExtension extends CompilerExtension
{

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$builder->getDefinition($builder->getByType(IPresenterFactory::class))->addSetup(
			'setMapping',
			[['GoogleTagManager' => 'Redenge\GoogleTagManager\AdminModule\Presenters\*Presenter']]
		);

		/*$this->compiler->loadDefinitions(
			$builder,
			$this->loadFromFile(__DIR__ . '/config.neon')['services']
		);*/
	}

}
