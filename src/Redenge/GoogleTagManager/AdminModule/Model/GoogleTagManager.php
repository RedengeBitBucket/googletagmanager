<?php

namespace Redenge\GoogleTagManager\AdminModule\Model;

use DBObject;


/**
 * Description of GoogleTagManager
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class GoogleTagManager extends DBObject
{

	/**
	 * @param \Database $database
	 * @param \DBObject $parentNode
	 */
	public function __construct($database = NULL, $parentNode = NULL)
	{
		if ($database) {
			parent::__construct($database, 'google_tag_manager', $parentNode);
		} else {
			$this->createInteger('id');
		}

		$this->createString('head');
		$this->createString('body');
		$this->createString('layer');
		$this->createInteger('id_multishop');
		$this->createInteger('id_country');
	}

}

