<?php

namespace Redenge\GoogleTagManager\FrontModule\Helper;


class PageType
{
	const   HOMEPAGE = 'home',
			CART = 'cart',
			PURCHASE = 'purchase',
			CATEGORY = 'category',
			SEARCH_RESULTS = 'searchresults',
			PRODUCT = 'product',
			STATIC_PAGE = 'other',
			STORE = 'other',
			OTHER = 'other';
}
