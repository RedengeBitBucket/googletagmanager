<?php

namespace Redenge\GoogleTagManager\FrontModule\Listener;

use Kdyby\Events\Subscriber;
use Latte\Engine;
use Latte\Loaders\StringLoader;
use Redenge\Base\Shop\Entity\OrderItem;
use Redenge\Base\Shop\Presenters\ICartPresenter;
use Redenge\Base\Shop\Presenters\IProductDetailPresenter;
use Redenge\Base\Shop\Presenters\IPurchasePresenter;
use Redenge\Base\Shop\View\IView;
use Redenge\GoogleTagManager\FrontModule\Helper\PageType;
use Redenge\GoogleTagManager\FrontModule\Layer;
use Redenge\GoogleTagManager\FrontModule\Property;


/**
 * Description of RenderListener
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class RenderListener implements Subscriber
{

	/**
	 * @var Layer
	 */
	private $layer;


	public function __construct(Layer $layer)
	{
		$this->layer = $layer;
	}


	/**
	 * {@inheritdoc}
	 */
	public function getSubscribedEvents()
	{
		return [
			'Redenge\Presenters::onBeforeRender',
			'Redenge\Presenters\Homepage::onRun' => 'onRunHomepage',
			'Redenge\Presenters\Account::onRun' => 'onRunAccount',
			'Redenge\Presenters\Payment::onRun' => 'onRunPayment',
			'Redenge\Presenters\ProductDetail::onRun' => 'onRunProductDetail',
			'Redenge\Presenters\ProductList::onRun' => 'onRunProductList',
			'Redenge\Presenters\Search::onRun' => 'onRunSearch',
			'Redenge\Presenters\StaticPage::onRun' => 'onRunStaticPage',
			'Redenge\Presenters\Store::onRun' => 'onRunStore',
			'Redenge\Presenters\Cart::onRun' => 'onRunCart',
			'Redenge\Presenters\Cart::onPaid' => 'onPaidCart',
		];
	}


	public function onRunHomepage()
	{
		$this->setPageType(PageType::HOMEPAGE);
	}


	public function onRunAccount()
	{
		$this->setPageType(PageType::OTHER);
	}


	public function onRunPayment(IPurchasePresenter $purchase)
	{
		$this->setPageType(PageType::PURCHASE);
		$this->setProductIds($purchase->getOrderVariantIds());
		$this->setTotalValue($purchase->getOrderTotalPriceVat());
		$this->setTransactionId($purchase->getOrderNumber());
		$this->setTransactionTotal($purchase->getOrderTotalPrice());
		$this->setTransactionTax(round($purchase->getOrderTotalPriceVat() - $purchase->getOrderTotalPrice(), 2));
		$this->setTransactionShipping(round($purchase->getOrderShippingPrice(), 2));
		$this->setTransactionProducts($purchase->getOrderItems());
	}


	public function onRunProductDetail(IProductDetailPresenter $productDetail)
	{
		$this->setPageType(PageType::PRODUCT);
		$this->setProductId($productDetail->getVariantId());
		$this->setTotalValue($productDetail->getPriceVat());
	}


	public function onRunProductList()
	{
		$this->setPageType(PageType::CATEGORY);
	}


	public function onRunSearch()
	{
		$this->setPageType(PageType::SEARCH_RESULTS);
	}


	public function onRunStaticPage()
	{
		$this->setPageType(PageType::STATIC_PAGE);
	}


	public function onRunStore()
	{
		$this->setPageType(PageType::STORE);
	}


	public function onRunCart(ICartPresenter $cart)
	{
		$this->setPageType(PageType::CART);
		$this->setProductIds($cart->getVariantIds());
		$this->setTotalValue($cart->getTotalPriceVat());
	}


	public function onPaidCart(IPurchasePresenter $purchase)
	{
		$this->setPageType(PageType::PURCHASE);
		$this->setProductIds($purchase->getOrderVariantIds());
		$this->setTotalValue($purchase->getOrderTotalPriceVat());
		$this->setTransactionId($purchase->getOrderNumber());
		$this->setTransactionTotal($purchase->getOrderTotalPrice());
		$this->setTransactionTax(round($purchase->getOrderTotalPriceVat() - $purchase->getOrderTotalPrice(), 2));
		$this->setTransactionShipping(round($purchase->getOrderShippingPrice(), 2));
		$this->setTransactionProducts($purchase->getOrderItems());
	}


	private function setPageType($pageType)
	{
		$this->layer->extend()->addProperty(Property::PAGE_TYPE)->setValueString($pageType);
	}


	private function setProductId($productId)
	{
		$this->layer->extend()->addProperty(Property::PRODUCT_ID)->setValueNumeric($productId);
	}


	private function setProductIds($productIds)
	{
		$this->layer->extend()->addProperty(Property::PRODUCT_ID)->setValueArray($productIds);
	}


	private function setTotalValue($value)
	{
		$this->layer->extend()->addProperty(Property::TOTAL_VALUE)->setValueNumeric($value);
	}


	/**
	 * ID (kód) objednávky
	 *
	 * @param string $transactionId
	 */
	private function setTransactionId($transactionId)
	{
		$this->layer->extend()->addProperty(Property::TRANSACTION_ID)->setValueString($transactionId);
	}


	private function setTransactionAffiliation($transactionAffiliation)
	{
		$this->layer->extend()->addProperty(Property::TRANSACTION_AFFILIATION)->setValueString($transactionAffiliation);
	}


	private function setTransactionTotal($transactionTotal)
	{
		$this->layer->extend()->addProperty(Property::TRANSACTION_TOTAL)->setValueNumeric($transactionTotal);
	}


	private function setTransactionTax($transactionTax)
	{
		$this->layer->extend()->addProperty(Property::TRANSACTION_TAX)->setValueNumeric($transactionTax);
	}


	private function setTransactionShipping($transactionShipping)
	{
		$this->layer->extend()->addProperty(Property::TRANSACTION_SHIPPING)->setValueNumeric($transactionShipping);
	}


	/**
	 * @param array $transactionProducts
	 */
	private function setTransactionProducts(array $transactionProducts)
	{
		if (empty($transactionProducts)) {
			return;
		}
		
		$productsArray = $this->layer->extend()->addProperty(Property::TRANSACTION_PRODUCTS)->setValueArrayObject();
		foreach ($transactionProducts as $index => $orderItem) {
			/* @var $orderItem OrderItem */
			$object = $productsArray->addObject();
			$object->addProperty('sku')->setValueString($orderItem->getSku());
			$object->addProperty('name')->setValueString($orderItem->getName());
			$object->addProperty('category')->setValueString($orderItem->getCategory());
			$object->addProperty('price')->setValueNumeric($orderItem->getPrice());
			$object->addProperty('quantity')->setValueNumeric($orderItem->getQuantity());
		}
	}


	public function onBeforeRender(IView $view)
	{
		$elementHead = 'GTM_HEAD';
		$elementBody = 'GTM_BODY';
		$dataLayer = 'GTM_DATALAYER';

		$view->addToView($elementHead, $this->renderHead());
		$view->addToView($elementBody, $this->renderBody());
		$view->addToView($dataLayer, $this->renderLayer());
	}


	private function renderHead()
	{
		return $this->layer->getSettings()->getHead();
	}


	private function renderBody()
	{
		return $this->layer->getSettings()->getBody();
	}


	private function renderLayer()
	{
		$layerTemplate = $this->layer->getSettings()->getLayerTemplate();
		$latte = new Engine;
		$latte->setLoader(new StringLoader([
			'main' => $layerTemplate
		]));

		$params = [];
		foreach ($this->layer->getLayer() as $object) {
			foreach ($object as $property) {
				$value = $property->getValue();
				if ($value instanceof \Redenge\GoogleTagManager\FrontModule\ArrayObject) {
					$array = [];
					foreach ($value as $component) {
						$item = new \stdClass();
						foreach ($component as $componentProperty) {
							$item->{$componentProperty->getKey()} = $componentProperty->getValue();
						}
						$array[] = $item;
					}
					$params[$property->getKey()] = $array;
				} else {
					$params[$property->getKey()] = $value;
				}
			}
		}

		return $latte->renderToString('main', $params);
	}

}
