<?php

namespace Redenge\GoogleTagManager\FrontModule;


/**
 * Description of LayerFactory
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class LayerFactory
{

	/**
	 * @var Layer
	 */
    protected static $instance;


	private function __construct()
	{
	}


	/**
	 * @return Layer
	 */
	public static function create(\ShopModel $model)
	{
		if (!self::$instance) {
			$settings = (new Settings)->loadFromModel($model);
			self::$instance = new Layer($settings);
		}

		return self::$instance;
	}

}
