<?php

namespace Redenge\GoogleTagManager\FrontModule;


/**
 * Description of TagManager
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Layer
{

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var ArrayObject
	 */
	private $layer;

	/**
	 * @var Object
	 */
	private $object;


	public function __construct(Settings $settings)
	{
		$this->settings = $settings;
		$this->layer = $array = new ArrayObject();
		$this->object = $array->addObject();
	}


	/**
	 * @return Settings
	 */
	public function getSettings()
	{
		return $this->settings;
	}


	/**
	 * @internal
	 * @return ArrayObject
	 */
	public function getLayer()
	{
		return $this->layer;
	}


	/**
	 * @return Object
	 */
	public function extend()
	{
		return $this->object;
	}

}
