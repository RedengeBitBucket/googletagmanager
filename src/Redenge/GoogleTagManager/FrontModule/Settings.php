<?php

namespace Redenge\GoogleTagManager\FrontModule;

use Redenge\GoogleTagManager\AdminModule\Model\GoogleTagManager;


/**
 * Description of Settings
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Settings
{

	/**
	 * @var string
	 */
	private $head;

	/**
	 * @var string
	 */
	private $body;

	/**
	 * @var string
	 */
	private $layerTemplate;


	/**
	 * @param GoogleTagManager $model
	 * @return void
	 */
	public function loadFromModel(\ShopModel $model)
	{
		$model->google_tag_manager->load_by([
			'id_multishop' => $model->multishop->id,
			'id_country' => $model->country->id,
		]);
		$this->loadFromArray($model->google_tag_manager->getArray());

		return $this;
	}


	/**
	 * @param array $settings
	 * @return Settings
	 */
	public function loadFromArray($settings)
	{
		$this->head = isset($settings['head']) ? $settings['head'] : '';
		$this->body = isset($settings['body']) ? $settings['body'] : '';
		$this->layerTemplate = isset($settings['layer']) ? $settings['layer'] : '';

		return $this;
	}


	/**
	 * @return string
	 */
	public function getHead()
	{
		return $this->head;
	}


	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}


	/**
	 * @return string
	 */
	public function getLayerTemplate()
	{
		return $this->layerTemplate;
	}


}
