<?php

namespace Redenge\GoogleTagManager\FrontModule;

use ArrayIterator;
use IteratorAggregate;


class Object implements IteratorAggregate
{

	/**
	 * @var array
	 */
	private $components = [];


	/**
	 * @param string $key
	 *
	 * @return Property
	 */
	public function addProperty($key)
	{
		$this->components[] = $property = new Property($key);

		return $property;
	}

	/**
	 * @return ArrayIterator
	 */
	public function getIterator()
	{
		return new ArrayIterator($this->components);
	}

}
