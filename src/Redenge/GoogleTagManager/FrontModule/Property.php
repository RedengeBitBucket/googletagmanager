<?php

namespace Redenge\GoogleTagManager\FrontModule;


class Property
{

	const   PRODUCT_ID = 'ecomm_prodid',
			PAGE_TYPE = 'ecomm_pagetype',
			TOTAL_VALUE = 'ecomm_totalvalue',
			PRODUCT_PRICE = 'ecomm_priceproduct',
			CATEGORY_NAME = 'category_name',
			PRODUCT_NAME = 'product_name',
			TRANSACTION_ID = 'transactionId',
			TRANSACTION_AFFILIATION = 'transactionAffiliation',
			TRANSACTION_TOTAL = 'transactionTotal',
			TRANSACTION_TAX = 'transactionTax',
			TRANSACTION_SHIPPING = 'transactionShipping',
			TRANSACTION_PRODUCTS = 'transactionProducts';

	/**
	 * @var string
	 */
	private $key;

	/**
	 * @var string|array|ArrayObject
	 */
	private $value;


	/**
	 * @param string $key
	 */
	public function __construct($key)
	{
		$this->key = $key;
	}


	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}


	/**
	 * @param float $value
	 */
	public function setValueNumeric($value)
	{
		$this->value = (float) $value;
	}


	/**
	 * @param string $value
	 */
	public function setValueString($value)
	{
		$this->value = (string) str_replace("'", "\\'", $value);
	}


	/**
	 * @param array $array
	 */
	public function setValueArray(array $array)
	{
		foreach ($array as $key => $item) {
			$array[$key] = is_numeric($item) ? $item : str_replace("'", "\\'", $item);
		}

		$this->value = $array;
	}


	/**
	 * @return ArrayObject
	 */
	public function setValueArrayObject()
	{
		$this->value = new ArrayObject();

		return $this->value;
	}

	/**
	 * @return string|array|ArrayObject
	 */
	public function getValue()
	{
		return $this->value;
	}

}
