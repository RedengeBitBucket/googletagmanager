<?php

namespace Redenge\GoogleTagManager\FrontModule;

use ArrayIterator;
use IteratorAggregate;


class ArrayObject implements IteratorAggregate
{

	/**
	 * @var Object[]
	 */
	private $components = [];


	/**
	 * @return Object
	 */
	public function addObject()
	{
		$this->components[] = $object = new Object();

		return $object;
	}


	public function getIterator()
	{
		return new ArrayIterator($this->components);
	}

}
