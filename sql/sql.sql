DROP TABLE IF EXISTS `google_tag_manager`;
CREATE TABLE `google_tag_manager` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head` text COLLATE utf8_czech_ci NOT NULL,
  `body` text COLLATE utf8_czech_ci NOT NULL,
  `layer` text COLLATE utf8_czech_ci NOT NULL,
  `id_multishop` int(10) unsigned NOT NULL,
  `id_country` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_multishop` (`id_multishop`),
  KEY `id_country` (`id_country`),
  CONSTRAINT `google_tag_manager_ibfk_1` FOREIGN KEY (`id_multishop`) REFERENCES `multishop` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `google_tag_manager_ibfk_2` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
